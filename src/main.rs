extern crate futures;
extern crate thrussh;
extern crate thrussh_keys;
extern crate tokio;
use log::{Record, Level, Metadata, LevelFilter};
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use thrussh::server::{Auth, Session};
use thrussh::*;
use thrussh_keys::*;

struct SimpleLogger;

impl log::Log for SimpleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Info
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("{} - {}", record.level(), record.args());
        }
    }

    fn flush(&self) {}
}

fn main() {
    log::set_boxed_logger(Box::new(SimpleLogger))
        .map(|()| log::set_max_level(LevelFilter::Trace)).unwrap();
    println!("Set logger to level 'trace'");
    async { a_main() };
}

async fn a_main() {

    let mut config = thrussh::server::Config::default();
    config.methods = MethodSet::PUBLICKEY;
    config.auth_rejection_time = std::time::Duration::from_secs(3);
    config.preferred.key = &[key::Name("rsa-sha2-256"), key::Name("rsa-sha2-512")];
    config
        .keys
        .push(load_secret_key("xtomp.key", None).unwrap());
    let config = Arc::new(config);
    let sh = Server {
        clients: Arc::new(Mutex::new(HashMap::new())),
        id: 0,
    };
    tokio::time::timeout(
        std::time::Duration::from_secs(120),
        thrussh::server::run(config, "0.0.0.0:2222", sh),
    )
    .await
    .unwrap_or(Ok(()))
    .unwrap();
}

#[derive(Clone)]
struct Server {
    clients: Arc<Mutex<HashMap<(usize, ChannelId), thrussh::server::Handle>>>,
    id: usize,
}

impl server::Server for Server {
    type Handler = Self;
    fn new(&mut self, addr: Option<std::net::SocketAddr>) -> Self {
        let s = self.clone();
        self.id += 1;
        println!("Got new client: {} from {:?}", self.id, addr);
        s
    }
}

impl server::Handler for Server {
    type FutureAuth = futures::future::Ready<Result<server::Auth, failure::Error>>;
    type FutureUnit = futures::future::Ready<Result<(), failure::Error>>;
    type FutureBool = futures::future::Ready<Result<bool, failure::Error>>;

    fn finished_auth(&mut self, auth: Auth) -> Self::FutureAuth {
        println!("finished auth: {:?}", auth);
        futures::future::ready(Ok(auth))
    }
    fn finished_bool(&mut self, b: bool, _s: &mut Session) -> Self::FutureBool {
        println!("finished bool: {}", b);
        futures::future::ready(Ok(b))
    }
    fn finished(&mut self, _s: &mut Session) -> Self::FutureUnit {
        println!("finished");
        futures::future::ready(Ok(()))
    }

    fn auth_publickey(&mut self, user: &str, key: &key::PublicKey) -> Self::FutureAuth {
        println!("User {} uses pubkey", user);
        println!("  key: {:?}", key);
        self.finished_auth(server::Auth::Accept)
    }
    fn data(&mut self, channel: ChannelId, data: &[u8], session: &mut Session) -> Self::FutureUnit {
        println!("data: {:?}", data);
        session.data(channel, data);
        self.finished(session)
    }
    fn channel_open_session(
        &mut self,
        channel: ChannelId,
        session: &mut Session,
    ) -> Self::FutureUnit {
        {
            let mut clients = self.clients.lock().unwrap();
            clients.insert((self.id, channel), session.handle());
        }
        self.finished(session)
    }
}
